var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

var USERS_COLLECTION = 'users';

//TODO make it suitable for debug and runtime
var MONGODB_URL_CONNECTION = 'mongodb://localhost:27017/test';

//HARDCODED: query just by 'username'
exports.GetUser = function (username, onRequestDataCompleted, onRequestDataFailed) {
    MongoClient.connect(MONGODB_URL_CONNECTION, function (error, db) {
        if (error) {
            console.log(error);
            onRequestDataFailed(error);
            return;
        }

        var cursor = db.collection(USERS_COLLECTION).find({'username': username});

        cursor.toArray(function (err, doc) {
            if (err) {
                console.log(err);
                onRequestDataFailed(err);
            } else {
                assert(doc.length <= 1);
                console.log(doc);
                if (doc.length <= 0) {
                    onRequestDataFailed("There is no user with the 'username' as '" + username + "'.");
                } else {
                    //It has to be the only one!
                    onRequestDataCompleted(doc[0]);
                }
            }
            db.close();
        });
    });
};

exports.AddUser = function (user, onRequestDataCompleted, onRequestDataFailed) {
    MongoClient.connect(MONGODB_URL_CONNECTION, function (err, db) {
        db.collection(USERS_COLLECTION).insertOne(user,
            function (error, result) {
                if (error) {
                    console.log(error);
                    onRequestDataFailed(error);
                } else {
                    console.log("Inserted a document into the '" + USERS_COLLECTION + "' collection.")
                    onRequestDataCompleted(result);
                    db.close();
                }
            });
    });
};