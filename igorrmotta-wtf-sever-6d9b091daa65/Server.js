var express = require('express');
var app = express();
var bodyParser = require('body-parser');

// Modules
var Login = require('./Features/Login');
var Test = require('./Dev/Test');
var User = require('./Modules/User');

var Utils = require('./Utils/Utils');
var JsonCode = require('./Utils/JsonCode');

app.use(bodyParser());

app.get('/favicon.ico', function (req, res) {
    res.end();
});

app.get('/api/user', function (req, res) {
    var httpConnection = {};
    httpConnection.httpRequest = req;
    httpConnection.httpResponse = res;
    httpConnection.httpMethod = req.method.toUpperCase();
    httpConnection.payload = {};

    Utils.CallIfExists(User, httpConnection);
});

app.post('/api/user', function (req, res) {
    var httpConnection = {};
    httpConnection.httpRequest = req;
    httpConnection.httpResponse = res;
    httpConnection.httpMethod = req.method.toUpperCase();
    httpConnection.payload = {};

    Utils.CallIfExists(User, httpConnection);
});

app.post('/api/login', function (req, res) {
    Login.POST(req, res);
});

app.use(express.static('../igorrmotta-yingframework-273d8ba64fb5'));


app.listen(8080, function () {
    console.log('Server running at port 8080.');
});