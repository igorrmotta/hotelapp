/*
 /api/login

 - POST : authenticate user
 */

var MongoDB = require('../MongoDB');
var Utils = require('../Utils/Utils');
var JsonCode = require('../Utils/JsonCode');

/*
 * Requests
 */
var onRequestDataCompleted = function (res, jsonResults) {
    Utils.ResponseSuccess(res, jsonResults);
};

var onRequestDataFailed = function (res, strError, jsonErrorCode) {
    Utils.ResponseError(res, strError, jsonErrorCode);
};

exports.GET = function (req, res) {
    // Error
    onRequestDataFailed(res, "/api/login does not support GET. Try it using POST.", JsonCode.NOT_FOUND);
};

exports.POST = function (req, res) {
    console.log("Login -", (new Date()).toLocaleString());

    var username = req.body.username;
    if (!username || username == "") {
        onRequestDataFailed(res, "/api/login requires the key 'username'.", JsonCode.INVALID_LOGIN);
        return;
    }

    var password = req.body.password;
    if (!password || password == "") {
        onRequestDataFailed(res, "/api/login requires the key 'password'.", JsonCode.INVALID_LOGIN);
        return;
    }

    MongoDB.GetUser(username, function onSucceed(user) {
        if (password == user.password) {
            onRequestDataCompleted(res, user, JsonCode.SUCCESS);
        } else {
            onRequestDataFailed(res, "Invalid password!", JsonCode.UNAUTHORIZED);
        }
    }, function onFailed(err) {
        onRequestDataFailed(res, err, JsonCode.UNAUTHORIZED);
    });
};

/*
 * @void
 * @return onRequestDataFailed (strError, code)
 */
exports.PUT = function (req, res) {
    // Error
    onRequestDataFailed(res, "/api/login does not support PUT. Try it using POST.", JsonCode.NOT_FOUND);
};

/*
 * @void
 * @return onRequestDataFailed (strError, code)
 */
exports.DELETE = function (req, res) {
    // Error
    onRequestDataFailed(res, "/api/login does not support DELETE. Try it using POST.", JsonCode.NOT_FOUND);
};
