exports.ServerVersion = 0.2;

exports.dbHost = 'host';
exports.dbPort = '3306'; // dbPort
exports.dbUser = 'root';
exports.dbPassword = 'root';
exports.dbDatabase = 'wtf-devel';

exports.RelativeMediaPath = "../Media";

exports.MEDIA_TYPE = {
    Image: {value: 1, name: "Imagem"},
    Video: {value: 2, name: "Video"},
    Audio: {value: 3, name: "Audio"}
};

exports.TYPE = {
    Student: {value: 1, name: "Estudante"},
    Responsible: {value: 2, name: "Responsável"},
    Interested: {value: 3, name: "Interessado"},
    Educator: {value: 4, name: "Educador"},
    Administrator: {value: 5, name: "Administrador"},
    Institution: {value: 6, name: "Instituição"}
};