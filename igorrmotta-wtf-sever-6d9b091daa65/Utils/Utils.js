var QueryString = require('querystring');
var Config = require('../Config');
var URL = require('url')
var fs = require('fs');

var JsonCode = require('../Utils/JsonCode');

exports.IsActive = 1;
exports.IsNotActive = 0;

/*
 * @func Suppl_Student
 *
 */
exports.CreateSuppl_Student = function () {
    var suppData = {};
    suppData.idUser = -1;
    suppData.hashResponsible = '';
    suppData.hashInterested = '';
    suppData.parent1 = '';
    suppData.parent2 = '';
    suppData.weight = 0;
    suppData.height = 0;
    suppData.observation = '';
    suppData.active = this.IsNotActive;
    return suppData;
};

/*
 * @func Suppl_Institution
 *
 */
exports.CreateSuppl_Institution = function () {
    var suppData = {};
    suppData.idUser = -1;
    suppData.site = '';
    suppData.telephone2 = '';
    suppData.active = this.IsNotActive;
    return suppData;
};

/*
 * @func Suppl_Educator
 *
 */
exports.CreateSuppl_Educator = function () {
    var suppData = {};
    suppData.idUser = -1;
    suppData.observation = '';
    suppData.role = '';
    suppData.active = this.IsNotActive;
    return suppData;
};

/*
 * @func Suppl_Educator
 *
 */
exports.CreateSuppl_Administrator = function () {
    var suppData = {};
    suppData.idUser = -1;
    suppData.telephone2 = '';
    suppData.observation = '';
    suppData.role = '';
    suppData.active = this.IsNotActive;
    return suppData;
};

/*
 * @func Suppl_Interested
 *
 */
exports.CreateSuppl_Interested = function () {
    var suppData = {};
    suppData.idUser = -1;
    suppData.telephone2 = '';
    suppData.businessPhone = '';
    suppData.observation = '';
    suppData.active = this.IsNotActive;
    return suppData;
};

/*
 * @func Suppl_Responsible
 *
 */
exports.CreateSuppl_Responsible = function () {
    var suppData = {};
    suppData.idUser = -1;
    suppData.telephone2 = '';
    suppData.businessPhone = '';
    suppData.observation = '';
    suppData.active = this.IsNotActive;
    return suppData;
};

/*
 * @func Media
 *
 */
exports.CreateMedia = function () {
    var media = {};
    media.description = '';
    media.extensionMedia = '';
    media.nameMedia = '';
    media.typeMedia = -1;
    media.active = this.IsNotActive;
    return media;
};

/**
 * @return {number}
 */
exports.GetDateDiffMinutes = function (dateStart, dateEnd) {
    var diffMilliseconds = dateStart - dateEnd;
    return Math.round(((diffMilliseconds % 86400000) % 3600000) / 60000);
};

/**
 * @void
 */
exports.ResponseSuccess = function (res, result) {
    //If result comes empty, send an empty answer or whatever
    var jsonResult = JSON.stringify(result);
    var httpCode = 200; //Success
    console.log("-->> ResponseSuccess");
    exports.ResponseJson(res, jsonResult, httpCode);
};

/**
 * @void
 */
exports.ResponseError = function (res, strError, jsonErrorCode) {
    var jsonResponse = {Message: strError, Code: jsonErrorCode};
    var httpCode = 400;

    // Example
    switch (jsonErrorCode) {
        case JsonCode.UNAUTHORIZED:
        {
            httpCode = 401; // Unauthorized
            break;
        }
        case JsonCode.NOT_FOUND:
        {
            httpCode = 404; // Service Not Found
            break;
        }
        case JsonCode.INTERNAL:
        {
            httpCode = 500; // Internal Server Error
            break;
        }
    }

    console.log("-->> ResponseError: \n", jsonResponse);
    exports.ResponseJson(res, jsonResponse, httpCode);
};

/**
 * @void
 */
exports.ResponseJson = function (res, responseJson, httpCode) {
    res.set('Content-Type', 'application/json');
    res.status(httpCode);
    res.end(JSON.stringify(responseJson));
};

/**
 * @boolean
 */
exports.IsJsonString = function (str) {
    try {
        JSON.parse(str);
    }
    catch (e) {
        console.log(e);
        return false;
    }
    return true;
};

/**
 * @void
 */
exports.GetPostData = function (connection, onRequestDataCompleted, onRequestDataFailed) {
    var request = connection.httpRequest;
    var data = '';

    request.on('data', function (chunk) {
        data += chunk;
    });

    request.on('end', function () {
        onRequestDataCompleted(QueryString.parse(data));
    });

    request.on('error', function (err) {
        onRequestDataFailed(err);
    });
};


/**
 * @param {string} column
 * @param {array} array
 * @return {array}
 */
exports.GetColumnOfArray = function (column, array) {
    var retArray = [];
    for (var i = 0; i < array.length; i++) {
        if (array[i] && array[i][column]) {
            retArray.push(array[i][column]);
        }
    }
    retArray = retArray.filter(function (item, index, inputArray) {
        return inputArray.indexOf(item) == index;
    });
    return retArray;
};

exports.ParseCookies = function (request) {
    var list = {},
        rc = request.headers.cookie;

    console.log("List: \n", list);
    console.log("Header: \n", request.headers);

    rc && rc.split(';').forEach(function (cookie) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = unescape(parts.join('='));
    });
    console.log("Cookies:\n", list);
    return list;
};

/*!
 Math.uuid.js (v1.4)
 http://www.broofa.com
 mailto:robert@broofa.com

 Copyright (c) 2010 Robert Kieffer
 Dual licensed under the MIT and GPL licenses.
 */

/*
 * Generate a random uuid.
 *
 * USAGE: Math.uuid(length, radix)
 *   length - the desired number of characters
 *   radix  - the number of allowable values for each character.
 *
 * EXAMPLES:
 *   // No arguments  - returns RFC4122, version 4 ID
 *   >>> Math.uuid()
 *   "92329D39-6F5C-4520-ABFC-AAB64544E172"
 *
 *   // One argument - returns ID of the specified length
 *   >>> Math.uuid(15)     // 15 character ID (default base=62)
 *   "VcydxgltxrVZSTV"
 *
 *   // Two arguments - returns ID of the specified length, and radix. (Radix must be <= 62)
 *   >>> Math.uuid(8, 2)  // 8 character ID (base=2)
 *   "01001010"
 *   >>> Math.uuid(8, 10) // 8 character ID (base=10)
 *   "47473046"
 *   >>> Math.uuid(8, 16) // 8 character ID (base=16)
 *   "098F4D35"
 */
(function () {
    // Private array of chars to use
    var CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');

    Math.uuid = function (len, radix) {
        var chars = CHARS, uuid = [], i;
        radix = radix || chars.length;

        if (len) {
            // Compact form
            for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random() * radix];
        }
        else {
            // rfc4122, version 4 form
            var r;

            // rfc4122 requires these characters
            uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
            uuid[14] = '4';

            // Fill in random data.  At i==19 set the high bits of clock sequence as
            // per rfc4122, sec. 4.1.5
            for (i = 0; i < 36; i++) {
                if (!uuid[i]) {
                    r = 0 | Math.random() * 16;
                    uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
                }
            }
        }

        return uuid.join('');
    };

    // A more performant, but slightly bulkier, RFC4122v4 solution.  We boost performance
    // by minimizing calls to random()
    Math.uuidFast = function () {
        var chars = CHARS, uuid = new Array(36), rnd = 0, r;
        for (var i = 0; i < 36; i++) {
            if (i == 8 || i == 13 || i == 18 || i == 23) {
                uuid[i] = '-';
            }
            else if (i == 14) {
                uuid[i] = '4';
            }
            else {
                if (rnd <= 0x02) rnd = 0x2000000 + (Math.random() * 0x1000000) | 0;
                r = rnd & 0xf;
                rnd = rnd >> 4;
                uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
            }
        }
        return uuid.join('');
    };

    // A more compact, but less performant, RFC4122v4 solution:
    Math.uuidCompact = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };
})();


/**
 * @void
 */
function Dispath(serviceObj, connection) {
    if (serviceObj && serviceObj[connection.httpMethod]) {
        serviceObj[connection.httpMethod](connection);
    }
}

/**
 * @void
 */
exports.CallIfExists = function (serviceObj, connection) {
    //var Media = require('../Modules/Media');
    //if (serviceObj == Media) {
    if (connection.httpMethod == 'GET' || connection.httpMethod == 'DELETE') {
        //TODO check if DELETE and GET are not going to the same route
        connection.payload = URL.parse(connection.httpRequest.url, true)['query'];
        Dispath(serviceObj, connection);
    }
    //}
    else if (connection.httpMethod == 'POST' || connection.httpMethod == 'PUT') {
        var onRequestDataCompleted = function (data) {
            connection.payload = data;
            Dispath(serviceObj, connection);
        };

        var onRequestDataFailed = function () {
            console.log('Failed to Read POST/PUT data. HttpStatus 500');
            this.ResponseJson(connection, "Failed to Read POST/PUT data.", JsonCode.INTERNAL.value);
        };

        this.GetPostData(connection, onRequestDataCompleted, onRequestDataFailed);
    }
    else {
        connection.payload = URL.parse(connection.httpRequest.url, true)['query'];
        Dispath(serviceObj, connection);
    }
};

exports.IsValidString = function (string) {
    if (!string || string == '')
        return false;
    else
        return true;
};

exports.ResponseHTMLFile = function (file, response) {
    fs.readFile(file, function (err, data) {
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(data);
        response.end();
    });
};

exports.ResponseFile = function (file, response) {
    fs.readFile(file, function (err, data) {
        response.write(data);
        response.end();
    });
};

