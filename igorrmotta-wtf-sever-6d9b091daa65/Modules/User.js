/* Connection
 connection.httpRequest = request;
 connection.httpResponse = response;
 connection.httpMethod = request.method.toUpperCase();
 connection.payload = data;
 connection.isUserAuthenticated
 */

var MongoDB = require('../MongoDB');
var Utils = require('../Utils/Utils');

var JsonCode = require('../Utils/JsonCode');

var m_HttpConnection;

/*
 @return User
 */
var User = (function () {

    var _this = {};

    function User() {
        _this = this;

        _this.username = '';
        _this.password = '';
        _this.email = '';
    }

    User.prototype.isValid = function () {
        if (!Utils.IsValidString(_this.username) || !Utils.IsValidString(_this.password) || !Utils.IsValidString(_this.email)) {
            return false;
        }
        return true;
    };

    return User;
})();

/*
 * Requests
 */
var onRequestDataCompleted = function (jsonResults) {
    Utils.ResponseSuccess(m_HttpConnection, jsonResults);
};

var onRequestDataFailed = function (strError, code) {
    Utils.ResponseError(m_HttpConnection, strError, code);
};


/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 *      URL: /api/user?username=igorrmotta
 */
exports.GET = function (connection) {
    console.log("User GET -", (new Date()).toLocaleString());

    m_HttpConnection = connection;
    var payload = connection.payload;
    var username = payload['username'];
    console.log('Looking for user: ' + username);

    if (username) {
        MongoDB.GetUser(username, onRequestDataCompleted, onRequestDataFailed);
    }
    else {
        onRequestDataFailed("You must provide the parameter 'username'.", JsonCode.NOT_FOUND.value);
    }
};

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.POST = function (connection) {
    console.log("User POST -", (new Date()).toLocaleString());

    /*
     URL: /api/user
     */
    m_HttpConnection = connection;
    var payload = connection.payload;

    var user = new User();
    user.username = payload['username'];
    user.password = payload['password'];
    user.email = payload['email'];

    if (!user.isValid()) {
        onRequestDataFailed("User is not valid.", JsonCode.FAIL.value);
        return;
    }

    MongoDB.GetUser(user.username, function () {
        //If it succeed is because already there is an user with that username
        onRequestDataFailed("Username already used. Try another one.", JsonCode.FAIL.value);
    }, function (err) {
        MongoDB.AddUser(user, onRequestDataCompleted, onRequestDataFailed);
    });
};

exports.PUT = function (connection) {
    console.log("User PUT -", (new Date()).toLocaleString());

    /*
     URL: /api/user
     */
    m_HttpConnection = connection;


    // Error
    onRequestDataFailed("Not implemented.", JsonCode.INTERNAL.value);
};

exports.DELETE = function (connection) {
    console.log("User DELETE -", (new Date()).toLocaleString());

    /*
     URL: /api/user
     */
    m_HttpConnection = connection;


    // Error
    onRequestDataFailed("Not implemented.", JsonCode.INTERNAL.value);
};