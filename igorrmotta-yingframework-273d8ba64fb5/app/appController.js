"use strict";

angular.module('app').controller('appController',
    ['$scope', '$http', '$rootScope', '$cookies',
        function ($scope, $http, $rootScope) {
            $scope.state = 'unauthorized';
            $scope.errorMessage = '';
            $scope.user = {username: '', password: ''};

            $scope.signIn = function () {
                var data = {
                    username: $scope.user.username,
                    password: $scope.user.password
                };

                data = JSON.stringify(data);

                $http.post('/api/login', data).then(
                    function onSucceed(data, status, headers, config) {
                        $scope.state = 'authorized';
                        $rootScope.user = {
                            username: $scope.user.username
                        };
                    }, function onFailed(data, status, headers, config) {
                        if (data) {
                            $scope.state = 'unauthorized';
                            $scope.errorMessage = data.data['Message'];
                        }
                    });
            }
        }
    ]);
