"use strict";

angular.module('yingFramework').directive('yingUserProfileSmall', function () {
    return {
        transclude: true,
        scope: {
            username: '@'
        },
        templateUrl: 'ext-modules/yingFramework/yingUserProfile/yingUserProfileSmallTemplate.html',
    }
});