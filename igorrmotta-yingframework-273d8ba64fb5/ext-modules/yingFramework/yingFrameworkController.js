'use strict';

angular.module('yingFramework').controller('yingFrameworkController', ['$scope', '$window', '$timeout', '$rootScope',
    '$location',
    function ($scope, $window, $timeout, $rootScope, $location) {

        $scope.user = $rootScope.user;

        $scope.isMenuVisible = true;
        $scope.isMenuButtonVisible = true;
        $scope.isMenuVertical = true;

        $scope.$on('ying-menu-item-selected-event', function (evt, data) {
            $scope.routeString = data.route;
            $location.path(data.route);

            checkWidth();
            broadcastMenuState();
        });

        $scope.$on('ying-menu-orientation-changed-event', function (evt, data) {
            $scope.isMenuVertical = data.isMenuVertical;
            $timeout(function () {
                $($window).trigger('resize');
            }, 0);
        });

        $($window).on('resize.yingFramework', function () {
            $scope.$apply(function () {
                checkWidth();
                //TODO send event just when over the necessary
                broadcastMenuState();
            });
        });

        $scope.$on('$destroy', function () {
            $($window).off('resize.yingFramework'); //remove the handler
        });

        var checkWidth = function () {
            var width = Math.max($($window).width(), $window.innerWidth);
            $scope.isMenuVisible = (width >= 768);
            $scope.isMenuButtonVisible = !$scope.isMenuVisible;
        };

        $scope.menuButtonClicked = function () {
            $scope.isMenuVisible = !$scope.isMenuVisible;
            broadcastMenuState();
            //$scope.$apply();
        };

        var broadcastMenuState = function () {
            $rootScope.$broadcast('ying-menu-show',
                {
                    show: $scope.isMenuVisible,
                    isVertical: $scope.isMenuVertical,
                    allowHorizontalToggle: !$scope.isMenuButtonVisible
                });
        };

        $timeout(function () {
            checkWidth();
        }, 0);
    }]);