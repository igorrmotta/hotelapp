"use strict";

angular.module('yingMenu').directive('yingMenuGroup', function () {
    return {
        require: '^?yingMenu',
        transclude: true,
        scope: {
            label: '@',
            icon: '@'
        },
        templateUrl: 'ext-modules/yingMenu/yingMenuGroupTemplate.html',
        link: function (scope, el, attr, ctrl) {
            scope.isOpen = false;

            scope.closeMenu = function () {
                scope.isOpen = false;
            };

            scope.clicked = function () {
                scope.isOpen = !scope.isOpen;

                if (el.parents('.ying-subitem-section').length == 0)
                    scope.setSubmenuPosition();

                ctrl.setOpenMenuScope(scope);
            };

            scope.isVertical = function () {
                return ctrl.isVertical();
            };

            scope.setSubmenuPosition = function () {
                var pos = el.offset();
                $('.ying-subitem-section').css({'left': pos.left + 20, 'top': 36});
            }
        }
    }
});