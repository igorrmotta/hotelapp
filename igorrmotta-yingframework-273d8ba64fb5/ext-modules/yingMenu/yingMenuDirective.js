"use strict";

angular.module('yingMenu').directive('yingMenu', ['$timeout',
    function ($timeout) {
        return {
            restrict: 'E',
            scope: {},
            transclude: true,
            templateUrl: 'ext-modules/yingMenu/yingMenuTemplate.html',
            controller: 'yingMenuController',
            link: function (scope, el, attr) {
                var item = el.find('.ying-selectable-item:first');

                $timeout(function () {
                    item.trigger('click');
                });
            }
        }
    }
]);